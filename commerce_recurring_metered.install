<?php

/**
 * @file
 * Install, update and uninstall functions commerce_recurring_metered.
 */

/**
 * Implements hook_schema().
 */
function commerce_recurring_metered_schema() {
  $schema['commerce_recurring_usage'] = [
    'description' => 'Tracks subscription usage.',
    'fields' => [
      'usage_id' => [
        'description' => 'The primary key.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'usage_type' => [
        'description' => 'The ID of the usage type plugin providing tracking.',
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'subscription_id' => [
        'description' => 'The subscription ID.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'product_variation_id' => [
        'description' => 'The product variation ID for this record.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'quantity' => [
        'description' => 'The usage quantity.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'start' => [
        'description' => 'The Unix timestamp when usage began.',
        'type' => 'int',
        'not null' => FALSE,
      ],
      'end' => [
        'description' => 'The Unix timestamp when usage ended.',
        'type' => 'int',
        'not null' => FALSE,
      ],
    ],
    'primary key' => ['usage_id'],
    'indexes' => [
      'combined' => ['usage_type', 'subscription_id'],
      'timing' => ['start', 'end'],
    ],
  ];

  return $schema;
}
